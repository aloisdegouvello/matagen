# Matagen

Matagen is a basic Markdown Tables Generator

## Name

No link to chemistry's [Matagen](https://medical-dictionary.thefreedictionary.com/matagen). Here Matagen stands for **Ma**rkdown **Ta**ble **Gen**erator.

## Demo

[live](https://aloisdegouvello.gitlab.io/matagen/)